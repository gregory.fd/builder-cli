# Используем официальный образ Docker-in-Docker
FROM docker:26-dind

# Устанавливаем необходимые пакеты для установки Node.js
RUN apk add --no-cache nodejs npm bash jq

# Проверяем установку
RUN node --version && npm --version

COPY ./dist /builder
COPY ./package.json /builder/package.json
WORKDIR /app

CMD ["node", "/builder/src/Build.js"]
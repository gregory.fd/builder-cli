import { Project } from '../Project/index.js';
type Options = {
  parse_mode?: 'Markdown' | 'HTML' | 'MarkdownV2';
};

export const Telegram = new (class {
  botToken = process.env.TELEGRAM_BOT_TOKEN;
  chatId = process.env.TELEGRAM_CHAT_ID;

  async sendMessage(message: string, options: Options = {}) {
    const projectInfo = await Project.getInfo();
    const jobUrl = process.env.CI_JOB_URL ? `${projectInfo.name}:${projectInfo.version} Job URL: ${process.env.CI_JOB_URL}\n` : '';
    const params = new URLSearchParams({
      chat_id: String(this.chatId),
      text: (jobUrl + message).substring(0, 4000),
      ...(options.parse_mode && { parse_mode: options.parse_mode }),
    });
    const url = `https://api.telegram.org/bot${this.botToken}/sendMessage?${params.toString()}`;
    const response = await fetch(url);
    if (!response.ok) {
      const data = await response.text().catch(() => response.statusText);
      throw new Error(`Error when sending to Telegram ${data}`);
    }
  }

  async sendError(error: unknown) {
    const message = String(error);
    const projectInfo = await Project.getInfo();
    await this.sendMessage(`Build Error in ${projectInfo.name} ${projectInfo.tag_name}:\n${message}`);
  }
})();

import { ChildProcess } from 'child_process';
import { Telegram } from '../Telegram/index.js';

export const runScript = async (script: ChildProcess) => {
  script.on('error', (e) => Telegram.sendError(e));
  return new Promise((resolve, reject) => {
    script.on('close', (code) => {
      if (code === 0) resolve('');
      else reject(new Error(`Script exited with code ${code}`));
    });
  });
};

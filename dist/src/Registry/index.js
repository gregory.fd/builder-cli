import { runScript } from '../Utils/index.js';
import { spawn, spawnSync } from 'child_process';
const registry = process.env.CONTAINER_REGISTRY_URL;
if (!registry)
    throw new Error('GitLab variable CONTAINER_REGISTRY_URL is not defined');
export const Registry = new (class {
    url = registry;
    async checkVersionExists(tagName) {
        const tag = `${registry}/${tagName}`;
        const { stderr } = spawnSync('docker', ['manifest', 'inspect', tag]);
        if (stderr.toString().includes('no such manifest:'))
            return false;
        return true;
    }
    async build(tag) {
        const build = spawn('docker', ['build', '--push', '-t', `${registry}/${tag}`, '--provenance=false', '.'], {
            stdio: 'inherit',
        });
        return runScript(build);
    }
})();

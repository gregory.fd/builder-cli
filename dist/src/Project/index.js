import { readFile } from 'node:fs/promises';
export const Project = new (class {
    async getInfo() {
        const packageFile = await readFile('package.json', 'utf-8');
        const packageJson = JSON.parse(packageFile);
        if (!packageJson.name)
            throw new Error('No name in package.json');
        if (!packageJson.version)
            throw new Error('No version in package.json');
        const name = packageJson.name;
        const version = packageJson.version;
        const tag_name = `${name}:${version}`;
        return { name, version, tag_name };
    }
})();

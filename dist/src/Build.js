import { existsSync } from 'node:fs';
import { Project } from './Project/index.js';
import { Registry } from './Registry/index.js';
import { runScript } from './Utils/index.js';
import { spawn } from 'node:child_process';
import { Telegram } from './Telegram/index.js';
const projectInfo = await Project.getInfo();
try {
    await Telegram.sendMessage(`Start building ${projectInfo.name} ${projectInfo.tag_name}`);
    const exists = await Registry.checkVersionExists(projectInfo.tag_name);
    if (exists)
        throw new Error(`Image ${projectInfo.tag_name} already exists`);
    const prebuildScript = existsSync('./prebuild.sh');
    if (prebuildScript)
        await runScript(spawn('bash', ['./prebuild.sh'], { stdio: 'inherit' }));
    await Registry.build(projectInfo.tag_name);
    await Telegram.sendMessage(`💃 Image ${Registry.url}/${projectInfo.tag_name} built successfully`);
}
catch (e) {
    await Telegram.sendError(e);
    throw e;
}

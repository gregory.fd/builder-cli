# Инструкция по запуску
Создать в репозитории файл `.gitlab-ci.yml`
```yml
image: cr.yandex/crpqrl3o3gvh2fc354rl/builder-cli:1.0.5

stages:
  - build

build:
  stage: build
  script:
    - docker login -u oauth -p $CI_REGISTRY_PASSWORD cr.yandex
    - node /builder/src/Build.js
  only:
    - tags
  tags:
    - build
```

#### TODO:
- Убрать дополнительную авторизацию и пробрасывать файл авторизации
- Добавить тег :latest


## Запуск сборки репозитория
```bash
npm version patch
git push origin --tags
```

## Создание GitLab Runner

### Добавить переменные окружения
GitLba -> Setting -> CI/CD -> Variables<br>
[Ссылка в настройки для группы LessCode]([Variables](https://gitlab.com/less-code-tech/mvp/sound-maker/-/settings/ci_cd))
-  `CONTAINER_REGISTRY_URL` = cr.yandex/crpqrl3o3gvh2fc354rl
-  `CI_REGISTRY_PASSWORD`
-  `TELEGRAM_BOT_TOKEN`
-  `TELEGRAM_CHAT_ID`


#### Запуск контейнера
```bash
docker run -d --name gitlab-runner --restart always \
  -v /work/docker/runner/config:/etc/gitlab-runner \
  -v /work/docker/runner/.docker:/root/.docker \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

## Конфиг файлы
`config/config.toml`
```toml
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "builder"
  output_limit = 409600
  url = "https://gitlab.com/"
  id = 38417304
  token = "glrt-e2-YQQU4sUtxbSvzJ9Ho"
  token_obtained_at = 2024-06-11T07:41:01Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  shell = "bash"
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/certs/client",  "/root/.docker:/root/.docker", "/var/run/docker.sock:/var/run/docker.sock"]
    pull_policy = ["always"]
    shm_size = 2147483648
    network_mtu = 0
  	allowed_pull_policies = ["always", "if-not-present"]

 ```

####  .docker/config.json  (этот файл можно достать из ~/.docker/config.json)
```
{
	"auths": {
		"cr.yandex": {
			"auth": "authToken"
		}
	}
}
```

#### Создание `authToken` токена
```
echo -n 'user:password' | base64
```


# Сборка образа builder-cli
```
docker build --push -t cr.yandex/crpqrl3o3gvh2fc354rl/builder-cli:1.0.4 --provenance=false .
```